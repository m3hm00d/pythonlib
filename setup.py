#!/usr/bin/python3

from setuptools import setup

setup(name='Tailslib',
      version='0.1',
      description='Tails python library',
      author='Tails developers',
      author_email='tails@boum.org',
      url='https://tails.boum.org/',
      license='GPLv3+',
      provides=['tailslib'],
      packages=['tailslib'],
     )
